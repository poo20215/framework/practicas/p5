<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of Usuarios
 *
 * @author Alpe
 */
class Modelo1 extends Model
{
    public $numero1;
    public $numero2;
    
    public function rules()
    {
        return[
            [['numero1','numero2'], 'required'],
            [['numero1','numero2'], 'number'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'numero1' => 'Numero 1',
            'numero2' => 'Numero 2',
        ];
    }
}

