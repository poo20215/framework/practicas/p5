<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Description of Usuarios
 *
 * @author Alpe
 */
class Modelo2 extends Model
{
    public $numero1;
    public $numero2;
    public $resultado;
    public $dividiendo=false;
    public $suma;
    public $resta;
    public $multiplicacion;
    public $division;
    
    
    
    public function rules()
    {
        return[
            [['numero1','numero2'], 'required'],
            [['numero1','numero2'], 'number'],
            ['numero2','dividirCero']        
        ];
    }
    
    public function dividirCero($attribute,$params)
    {
        if($this->$attribute==0)
        {
            if($this->dividiendo==true)
            {
                $this->addError($attribute,"No se puede dividir por 0");
            }
        }
    }
    
    public function attributeLabels()
    {
        return [
            'numero1' => 'Numero 1',
            'numero2' => 'Numero 2',
        ];
    }
    
    public function multiplicacion()
    {
        $this->multiplicacion=$this->numero1*$this->numero2;
        return $this->multiplicacion;
    }
        
    public function division()
    {
        if($this->numero2==0)
        {
            $this->division="No se puede dividir por 0";
        }else
        {
            $this->division=$this->numero1/$this->numero2;
            return $this->division;
        }
        
    }
    
    public function suma()
    {
        $this->suma=$this->numero1+$this->numero2;
        return $this->suma;
    }
    
    public function resta()
    {
        $this->resta=$this->numero1-$this->numero2;
        return $this->resta;
    }
                
}

