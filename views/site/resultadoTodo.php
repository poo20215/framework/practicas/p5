<?php
use yii\widgets\DetailView;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'numero1',
        'numero2',
        'suma',
        'resta',
        'multiplicacion',
        'division',
    ],
]);

