<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Modelo1;
use app\models\Modelo2;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
     public function actionSumar()
    {
        $model = new Modelo1();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $resultado=$model->numero1+$model->numero2;
            return $this->render('resultado1', ['resultado' => $resultado,'operacion'=>'suma']);
        } else 
        {
            // la página es mostrada inicialmente o hay algún error de validación
            return $this->render('formulario1', ['model' => $model,'boton' => 'Sumar']);
        }
    }
    
    public function actionRestar()
    {
        $model = new Modelo1();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $resultado=$model->numero1-$model->numero2;
            return $this->render('resultado1', ['resultado' => $resultado,'operacion'=>'resta']);
        } else 
        {
            // la página es mostrada inicialmente o hay algún error de validación
            return $this->render('formulario1', ['model' => $model,'boton' => 'Restar']);
        }
    }
    
    public function actionMultiplicar()
    {
        $model = new Modelo2();

        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $model->resultado=$model->multiplicacion();
                return $this->render('resultado2', ['model' => $model,'operacion' => 'multiplicacion']);

            }
        }
        
        return $this->render('formulario2', ['model' => $model,'boton' => 'Multiplicar']);
    }
    
    public function actionDividir()
    {
        $model = new Modelo2();
        $model->dividiendo=true;
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $model->resultado=$model->division();
                return $this->render('resultado2', ['model' => $model,'operacion' => 'division']);

            }
        }
        
        return $this->render('formulario2', ['model' => $model,'boton' => 'Dividir']);
    }
    
    public function actionOperartodo()
    {
        $model = new Modelo2();
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $model->suma();
                $model->resta();
                $model->multiplicacion();
                $model->division();
                
                return $this->render('resultadoTodo', ['model' => $model]);
            }
        }
        
        return $this->render('formulario2', ['model' => $model,'boton' => 'Operar todo']);
    }
    
}
